'use strict';
angular.module('view.component.footer', []);
angular.module('view.component.footer').component('cFooter', {
    templateUrl: 'views/components/footers/footer.html',
    controller: [ 'coreConfig',
        function (coreConfig) {
            var $ctrl = this;
            $ctrl.logoHead = coreConfig.logoHead;
        }
    ]
});
