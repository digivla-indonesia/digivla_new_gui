'use strict';
angular.module('view.component', [
    'view.component.footer',
    'view.component.form',
    'view.component.modal',
    'view.component.soundalert'
]);
