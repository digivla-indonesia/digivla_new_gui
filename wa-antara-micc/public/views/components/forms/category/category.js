'use strict';
angular.module('view.component.form.category', []);
angular.module('view.component.form.category').component('cFormCategory', {
    templateUrl: 'views/components/forms/category/category.html',
    controller: [
        function () {
            var $ctrl = this;
            $ctrl.prop = {
                radios: [
                    { index: '01', text: 'Apple' },
                    { index: '02', text: 'Apple' },
                    { index: '03', text: 'Apple' },
                    { index: '04', text: 'Apple' },
                    { index: '05', text: 'Apple' },
                    { index: '06', text: 'Apple' },
                    { index: '07', text: 'Apple' },
                    { index: '08', text: 'Apple' },
                    { index: '09', text: 'Apple' }
                ]
            };

        }
    ],
    bindings: {
        model:'=',
        parameters: '<'
    }
});
