'use strict';
angular.module('view.component.form', [
    'view.component.form.mediatype',
    'view.component.form.category',
    'view.component.form.timeframe'
]);
