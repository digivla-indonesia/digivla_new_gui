'use strict';
angular.module('view.component.form.timeframe', []);
angular.module('view.component.form.timeframe').component('cFormTimeframe', {
    templateUrl: 'views/components/forms/timeframe/timeframe.html',
    controller: ['$scope', '$uibModal',
        function ($scope, $uibModal) {
            var $ctrl = this;
            $ctrl.prop = {
                timeframes: [
                    { timeframe: 1, text: 'One Day', image: 'calendar-1-white.png', imageHover: 'calendar-1-primary.png' },
                    { timeframe: 7, text: 'Weekly', image: 'calendar-7-white.png', imageHover: 'calendar-7-primary.png' },
                    { timeframe: 30, text: 'Monthly', image: 'calendar-31-white.png', imageHover: 'calendar-31-primary.png' },
                    { timeframe: 365, text: 'Yearly', image: 'calendar-365-white.png', imageHover: 'calendar-365-primary.png' },
                    { timeframe: 0, text: 'Selected', image: 'history-white.png', imageHover: 'history-primary.png' }
                ]
            };
            $ctrl.state = {
                active: 'w'
            };

            // initiate page
            $ctrl.state.active = 7;
            $ctrl.model = {
                tf: 7,
                df: "",
                dt: ""
            };

            $scope.$watch(function(){
                return $ctrl.model;
            }, function(values){
                $ctrl.state.active = values.tf;
            });

            $ctrl.modalOpen = function (timeframe) {
                if(timeframe.timeframe == 0){
                    var modalInstance = $uibModal.open({
                        component: 'cModal',
                        resolve: {
                            modalConfig: function () {
                                return {
                                    title: timeframe.text
                                };
                            },
                            modalData: function () {
                                return {
                                    timeframe: timeframe
                                };
                            }
                        },
                        size: 'lg'
                    });

                    modalInstance.result.then(
                        function (result) {
                            // console.log(JSON.stringify(result));
                            $ctrl.model = {
                                tf: timeframe.timeframe,
                                df: result.from,
                                dt: result.to
                            };
                        },
                        function (reason) {
                            console.log(JSON.stringify(reason));
                        }
                    );
                } else{
                    // $ctrl.model = timeframe.timeframe;
                    $ctrl.model = {
                        tf: timeframe.timeframe,
                        df: "",
                        dt: ""
                    };
                }
                
                $ctrl.state.active = timeframe.timeframe;
            };
        }
    ],
    bindings: {
        model:'='
    }
});
