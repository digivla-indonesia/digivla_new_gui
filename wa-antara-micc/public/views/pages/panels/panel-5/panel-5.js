'use strict';
angular.module('view.page.panels.panel-5', ['synchroscope']);
angular.module('view.page.panels.panel-5').component('pPanel5', {
    templateUrl: 'views/pages/panels/panel-5/panel-5.html',
    controller: ['$window', '$scope', '$timeout', '$interval', 'coreHelperChart', 'modelMention', 'modelTest', 'modelHTTP', 'coreConfig', '$ync',
        function($window, $scope, $timeout, $interval, coreHelperChart, modelMention, modelTest, modelHTTP, coreConfig, $ync) {
            var $ctrl = this;
            $ctrl.periodSelection = {};
            $ctrl.model = {
                mention: {
                    chart:{
                        labels: [],
                        series: [],
                        data: []
                    }
                }
            };

            $timeout(function(){
                $scope.$watch(function(){
                    return $ctrl.categorySelection;
                }, function(){
                    $scope.categorySelection = $ctrl.categorySelection;
                    return changeFilter(true);
                });

                $scope.$watch(function(){
                    return $ctrl.mediaTypeSelection;
                }, function(){
                    $scope.mediaTypeSelection = $ctrl.mediaTypeSelection;
                    return changeFilter(true);
                });

                $scope.$watch(function(){
                    return $ctrl.periodSelection;
                }, function(){
                    $scope.periodSelection = $ctrl.periodSelection;
                    return changeFilter(true);
                });


                // between browsers
                $scope.$watch('categorySelection', function(newValues){
                    if ($ctrl.categorySelection !== newValues) {
                        $ctrl.categorySelection = newValues;
                        $scope.categorySelection = $ctrl.categorySelection;
                        return changeFilter(true);
                    }
                });

                $scope.$watch('mediaTypeSelection', function(newValues){
                    if ($ctrl.mediaTypeSelection !== newValues) {
                        $ctrl.mediaTypeSelection = newValues;
                        $scope.mediaTypeSelection = $ctrl.mediaTypeSelection;
                        return changeFilter(true);
                    }
                });

                $scope.$watch('periodSelection', function(newValues){
                    if ($ctrl.periodSelection !== newValues) {
                        $ctrl.periodSelection = newValues;
                        $scope.periodSelection = $ctrl.periodSelection;
                        return changeFilter(true);
                    }
                });

                // auto refresh
                $interval(function(){
                    $ctrl.model.mention.chart = {};
                    return changeFilter(true);
                }, coreConfig.app.refreshInterval);
                
            }, 5000);



            $ctrl.$onInit = function () {
                angular.element($window).bind('resize', onWindowResize);


                // ############################
                var categoryDatas = {
                    "url": "/client/getassets",
                    "method": "POST",
                    "data": {}
                };

                modelHTTP.HTTPPost('/api/digivla-requester', categoryDatas).success(function(response){
                    $ctrl.categorySet = response.data.data.data;
                    $ctrl.categorySelection = $ctrl.categorySet[0].category_set;

                    $scope.categorySelection = $ctrl.categorySelection;
                    $scope.mediaTypeSelection = $ctrl.mediaTypeSelection;
                    $scope.periodSelection = $ctrl.periodSelection;

                    changeFilter(false);
                });
            };


            // helper
            function onMentionChart(chart) {
                // Add global configuration
                $ctrl.model.mention.chart.options = angular.copy(coreConfig.chart.defaultOptions);
                // Add configurations for each line
                $ctrl.model.mention.chart.datasets = coreHelperChart.createDatasets(chart.data);
                // Fix chart font size
                $ctrl.model.mention.chart.options = coreHelperChart.fixFontSize($ctrl.model.mention.chart.options);
            }
            function refreshChartData(chart){
                $ctrl.model.mention.chart.datasets = coreHelperChart.createDatasets(chart.data);
            }

            function onWindowResize() {
                // Fix chart font size
                if ($ctrl.model.mention.chart.options) {
                    $ctrl.model.mention.chart.options = coreHelperChart.fixFontSize($ctrl.model.mention.chart.options);
                    $scope.$apply();
                }
            }
            function changeFilter(refresh){
                var datas = {
                    "url": "/formed/graph4",
                    "method": "POST",
                    "data": {
                        "gc": $ctrl.categorySelection,
                        "sc": "All Category",
                        "tf": $ctrl.periodSelection.tf,
                        "df": $ctrl.periodSelection.df,
                        "dt": $ctrl.periodSelection.dt,
                        "media_type": $ctrl.mediaTypeSelection
                    }
                };

                modelHTTP.HTTPPost('/api/digivla-requester', datas).success(function(response){
                    // $ctrl.model.mention.chart = response.data.data;
                    var datas = response.data.data;

                    $ctrl.model.mention.chart.labels = datas.labels;
                    $ctrl.model.mention.chart.series = datas.series;
                    $ctrl.model.mention.chart.data = datas.data;

                    if(refresh) return refreshChartData(response.data.data);
                    else return onMentionChart(response.data.data);

                });
            }

            $YNC.latencyTest = 100
            $ync($scope, ['categorySelection', 'periodSelection', 'mediaTypeSelection'], coreConfig.app.socketServer);
        }
    ]
});
