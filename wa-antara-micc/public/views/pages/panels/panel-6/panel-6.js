'use strict';
angular.module('view.page.panels.panel-6', ['synchroscope']);
angular.module('view.page.panels.panel-6').component('pPanel6', {
    templateUrl: 'views/pages/panels/panel-6/panel-6.html',
    controller: ['$window', '$scope', '$timeout', '$interval', 'coreConfig', 'coreHelperChart', 'modelMention', 'modelHTTP', '$ync',
        function($window, $scope, $timeout, $interval, coreConfig, coreHelperChart, modelMention, modelHTTP, $ync) {
            var $ctrl = this;
            $ctrl.model = {
                mention: {
                    doughnut: []
                }
            };

            $timeout(function(){
                $scope.$watch(function(){
                    return $ctrl.categorySelection;
                }, function(){
                    $scope.categorySelection = $ctrl.categorySelection;
                    changeFilter();
                });

                $scope.$watch(function(){
                    return $ctrl.mediaTypeSelection;
                }, function(){
                    $scope.mediaTypeSelection = $ctrl.mediaTypeSelection;
                    changeFilter();
                });

                $scope.$watch(function(){
                    return $ctrl.periodSelection;
                }, function(){
                    $scope.periodSelection = $ctrl.periodSelection;
                    changeFilter();
                });

                

                // between browsers
                $scope.$watch('categorySelection', function(newValues){
                    if ($ctrl.categorySelection !== newValues) {
                        $ctrl.categorySelection = newValues;
                        $scope.categorySelection = $ctrl.categorySelection;
                        changeFilter();
                    }
                });

                $scope.$watch('mediaTypeSelection', function(newValues){
                    if ($ctrl.mediaTypeSelection !== newValues) {
                        $ctrl.mediaTypeSelection = newValues;
                        $scope.mediaTypeSelection = $ctrl.mediaTypeSelection;
                        changeFilter();
                    }
                });

                $scope.$watch('periodSelection', function(newValues){
                    if ($ctrl.periodSelection !== newValues) {
                        $ctrl.periodSelection = newValues;
                        $scope.periodSelection = $ctrl.periodSelection;
                        changeFilter();
                    }
                });
            }, 5000);

            // auto refresh
            $interval(function(){
                $ctrl.model.mention.chart = {};
                changeFilter();
            }, coreConfig.app.refreshInterval);

            $ctrl.$onInit = function () {
                // get category
                var categoryDatas = {
                    "url": "/client/getassets",
                    "method": "POST",
                    "data": {}
                };

                modelHTTP.HTTPPost('/api/digivla-requester', categoryDatas).success(function(response){
                    $ctrl.categorySet = response.data.data.data;
                    $ctrl.categorySelection = $ctrl.categorySet[0].category_set;

                    $scope.categorySelection = $ctrl.categorySelection;
                    $scope.mediaTypeSelection = $ctrl.mediaTypeSelection;
                    $scope.periodSelection = $ctrl.periodSelection;

                    $timeout(function(){
                        changeFilter();
                    }, 1000);
                });


                // Get mention data
                // modelMention.getDoughnut(null, onMentionDoughnut);
                angular.element($window).bind('resize', onWindowResize);
            };
            function onMentionChart(chart) {
                // Add global configuration
                $ctrl.model.mention.chart.options = angular.copy(coreConfig.chart.defaultOptions);

                // Add stacked bar configuration
                $ctrl.model.mention.chart.options = $.extend(true,  $ctrl.model.mention.chart.options, {
                    scales: {
                        xAxes: [{
                            stacked: true,
                            gridLines: {
                                drawBorder: true,
                                offsetGridLines: false
                            },
                            ticks: {
                                display: true,
                                fontColor: 'rgba(254,254,254,1)'
                            }
                        }],
                        yAxes: [{
                            stacked: true,
                            gridLines: {
                                drawBorder: true,
                                offsetGridLines: false
                            },
                            ticks: {
                                display: true,
                                fontColor: 'rgba(254,254,254,1)'
                            }
                        }]
                    }
                });
                var datasetsOptions = {
                    fill: true,
                    lineTension: null,
                    borderWidth: 0,
                    pointBackgroundColor: null,
                    pointBorderColor: null,
                    colors: ['#2abc6e', '#63d6eb', '#ff0090']
                };
                // Add configurations for each bar
                $ctrl.model.mention.chart.datasets = coreHelperChart.createDatasets(chart.data, datasetsOptions, true);
                // Fix chart font size
                $ctrl.model.mention.chart.options = coreHelperChart.fixFontSize($ctrl.model.mention.chart.options);

                // console.log("data: " + JSON.stringify($ctrl.model.mention.chart.data));
                // console.log("labels: " + JSON.stringify($ctrl.model.mention.chart.labels));
                // console.log("series: " + JSON.stringify($ctrl.model.mention.chart.series));
            }
            function onMentionDoughnut(doughnut) {
                console.log(doughnut)
                var firstColor;
                var doughnutLimit = 5; // Limit doughnut data to render
                for(var i = 0; i < doughnutLimit; i++) {
                    $ctrl.model.mention.doughnut[i] = doughnut[i];
                    if(doughnut[i].labels[1].toString() === "1") firstColor = coreConfig.colors.positive;
                    if(doughnut[i].labels[1].toString() === "0") firstColor = coreConfig.colors.netral;
                    if(doughnut[i].labels[1].toString() === "-1") firstColor = coreConfig.colors.negative;

                    doughnut[i].colors = [firstColor, coreConfig.colors.others];
                    // Add default configurations for each doughnut
                    $ctrl.model.mention.doughnut[i].datasets = coreHelperChart.createDoughnutDatasets(doughnut[i].data);
                }
            }
            function onWindowResize() {
                // Fix chart font size
                if ($ctrl.model.mention.chart.options) {
                    $ctrl.model.mention.chart.options = coreHelperChart.fixFontSize($ctrl.model.mention.chart.options);
                    $scope.$apply();
                }
            }

            function changeFilter(){
                // get coverage tonality
                var datas = {
                    "url": "/formed/graph3",
                    "method": "POST",
                    "data": {
                        "gc": $ctrl.categorySelection,
                        "sc": "All Category",
                        "tf": $ctrl.periodSelection.tf,
                        "df": $ctrl.periodSelection.df,
                        "dt": $ctrl.periodSelection.dt,
                        "media_type": $ctrl.mediaTypeSelection,
                        "code": "018"
                    }
                };

                modelHTTP.HTTPPost('/api/digivla-requester', datas).success(function(response){
                    $ctrl.model.mention.chart = response.data.data;
                    onMentionChart($ctrl.model.mention.chart);
                }).error(function(response){
                    alert('error');
                });


                // get tone by media
                var datas2 = {
                    "url": "/formed/graph3",
                    "method": "POST",
                    "data": {
                        "gc": $ctrl.categorySelection,
                        "sc": "All Category",
                        "tf": $ctrl.periodSelection.tf,
                        "df": $ctrl.periodSelection.df,
                        "dt": $ctrl.periodSelection.dt,
                        "media_type": $ctrl.mediaTypeSelection,
                        "code": "050"
                    }
                };

                modelHTTP.HTTPPost('/api/digivla-requester', datas2).success(function(response){
                    onMentionDoughnut(shuffle(response.data.data));
                }).error(function(response){
                    alert('error');
                });
            }

            function shuffle(array) {
                var currentIndex = array.length, temporaryValue, randomIndex;

                // While there remain elements to shuffle...
                while (0 !== currentIndex) {

                    // Pick a remaining element...
                    randomIndex = Math.floor(Math.random() * currentIndex);
                    currentIndex -= 1;

                    // And swap it with the current element.
                    temporaryValue = array[currentIndex];
                    array[currentIndex] = array[randomIndex];
                    array[randomIndex] = temporaryValue;
                }

                return array;
            }

            $YNC.latencyTest = 100
            $ync($scope, ['categorySelection', 'periodSelection', 'mediaTypeSelection'], coreConfig.app.socketServer);
        }
    ]
});
