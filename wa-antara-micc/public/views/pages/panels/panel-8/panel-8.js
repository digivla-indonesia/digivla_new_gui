'use strict';
angular.module('view.page.panels.panel-8', []);
angular.module('view.page.panels.panel-8').component('pPanel8', {
    templateUrl: 'views/pages/panels/panel-8/panel-8.html',
    controller: ['$scope', '$document', '$element', '$timeout', '$interval', 'modelMention', 'modelTest', 'modelHTTP', 'coreConfig', '$ync',
        function($scope, $document, $element, $timeout,  $interval, modelMention, modelTest, modelHTTP, coreConfig, $ync) {
            var $ctrl = this;
            $ctrl.periodSelection = {};
            $ctrl.model = {
                post: {
                    posts: []
                }
            };
            $ctrl.prop = {
                mediaplayer: {
                    post: {}
                }
            };


            // list controller variables
            // $ctrl.resultsBucket;
            $ctrl.resultsToDisplay = [];
            // $ctrl.offset;



            $ctrl.$onInit = function () {
                // for (var i = 0; i < 25; i++) {
                //     if(i % 2 === 0) {
                //         $ctrl.model.post.posts.push({
                //             date: '2017-11-19',
                //             text: '[VIDEO] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas hendrerit mauris ut odio pretium efficitur.',
                //             src: 'http://techslides.com/demos/sample-videos/small.mp4',
                //             type: 'video'
                //         });
                //     }
                //     else {
                //         $ctrl.model.post.posts.push({
                //             date: '2017-11-19',
                //             text: '[PDF] Nullam et massa in lacus rutrum scelerisque. Nullam imperdiet ex et augue pretium, eget gravida purus sagittis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas hendrerit mauris ut odio pretium efficitur. ',
                //             src: 'https://pdfobject.com/pdf/sample-3pp.pdf',
                //             type: 'pdf'
                //         });
                //     }
                // }
                // $ctrl.prop.mediaplayer.post = {
                //     type: $ctrl.model.post.posts[0].type,
                //     src: $ctrl.model.post.posts[0].src
                // };

                // ############################
                var categoryDatas = {
                    "url": "/client/getassets",
                    "method": "POST",
                    "data": {}
                };

                modelHTTP.HTTPPost('/api/digivla-requester', categoryDatas).success(function(response){
                    $ctrl.categorySet = response.data.data.data;
                    $ctrl.categorySelection = $ctrl.categorySet[0].category_set;

                    $scope.categorySelection = $ctrl.categorySelection;
                    $scope.mediaTypeSelection = $ctrl.mediaTypeSelection;
                    $scope.periodSelection = $ctrl.periodSelection;

                    changeFilter();




                    // refresh and variable's watcher
                    $timeout(function(){
                        $scope.$watch(function(){
                            return $ctrl.categorySelection;
                        }, function(){
                            $scope.categorySelection = $ctrl.categorySelection;
                            return changeFilter();
                        });

                        $scope.$watch(function(){
                            return $ctrl.mediaTypeSelection;
                        }, function(){
                            $scope.mediaTypeSelection = $ctrl.mediaTypeSelection;
                            return changeFilter();
                        });

                        $scope.$watch(function(){
                            return $ctrl.periodSelection;
                        }, function(){
                            $scope.periodSelection = $ctrl.periodSelection;
                            return changeFilter();
                        });


                        // between browsers
                        $scope.$watch('categorySelection', function(newValues){
                            if ($ctrl.categorySelection !== newValues) {
                                $ctrl.categorySelection = newValues;
                                $scope.categorySelection = $ctrl.categorySelection;
                                return changeFilter();
                            }
                        });

                        $scope.$watch('mediaTypeSelection', function(newValues){
                            if ($ctrl.mediaTypeSelection !== newValues) {
                                $ctrl.mediaTypeSelection = newValues;
                                $scope.mediaTypeSelection = $ctrl.mediaTypeSelection;
                                return changeFilter();
                            }
                        });

                        $scope.$watch('periodSelection', function(newValues){
                            if ($ctrl.periodSelection !== newValues) {
                                $ctrl.periodSelection = newValues;
                                $scope.periodSelection = $ctrl.periodSelection;
                                return changeFilter();
                            }
                        });

                        // auto refresh
                        $interval(function(){
                            pushToList();


                            console.log('$ctrl.offset: ' + $ctrl.offset);
                            console.log('$ctrl.resultsToDisplay.length: ' + $ctrl.resultsToDisplay.length);
                            console.log('$ctrl.resultsBucket.length: ' + $ctrl.resultsBucket.length);
                        }, coreConfig.app.refreshInterval);
                        // }, 5000);
                        
                    }, 5000);


                });
            };
            $ctrl.setMediaplayer = function (post) {
                // var file = post.file_pdf,
                //     tanggal = file.substring(8, 10),
                //     bulan = file.substring(5, 7),
                //     tahun = file.substring(0, 4),
                //     // artikelOCR = 'http://pdf.antara-insight.id/pdf_text/' + tahun + '/' + bulan + '/' + tanggal + '/' + file,
                //     file_url = 'http://pdf.antara-insight.id/pdf_text/' + tahun + '/' + bulan + '/' + tanggal + '/' + file;

                $ctrl.prop.mediaplayer.post = {
                    type: post.type,
                    src: post.file_pdf
                };
            };

            function changeFilter(){
                var datas = {
                    "url": "/formed/getnewslist",
                    "method": "POST",
                    "data": {
                        "gc": $ctrl.categorySelection,
                        "sc": "All Category",
                        "tf": $ctrl.periodSelection.tf,
                        "df": $ctrl.periodSelection.df,
                        "dt": $ctrl.periodSelection.dt,
                        "media_type": $ctrl.mediaTypeSelection
                    }
                };

                modelHTTP.HTTPPost('/api/digivla-requester', datas).success(function(response){
                    // $ctrl.model.mention.chart = response.data.data;
                    // return onMentionChart($ctrl.model.mention.chart);
                    // console.log(JSON.stringify(response.data.data.data));
                    // console.log('banyak: ' + response.data.data.data.length);
                    $ctrl.model.post.posts = [];
                    $ctrl.resultsBucket = [];
                    $ctrl.resultsBucket = response.data.data.data;
                    $ctrl.offset = 0;

                    // $ctrl.prop.mediaplayer.post = {
                    //     type: response.data.data.tv.file_pdf,
                    //     src: 'VIDEO'
                    // };
                    var file = response.data.data.tv.file_pdf;
                    var tanggal = file.substring(8, 10);
                    var bulan = file.substring(5, 7);
                    var tahun = file.substring(0, 4);
                    var file_url = 'http://pdf.antara-insight.id/media_tv/' + tahun + '/' + bulan + '/' + tanggal + '/' + file;
                    console.log(file_url);
                    // var $ctrl.resultsBucket[i].detail.file_pdf = file_url;
                    $ctrl.setMediaplayer({src: file_url, type: 'video'});
                    pushToList();
                    // $ctrl.resultsToDisplay = [];

                });
            }

            function pushToList(resultsToDisplay, offset, resultsBucket){
                // $ctrl.model.post.posts = [];
                var tmpArr = [];
                var file, tanggal, bulan, tahun, file_url;
                


                if( ($ctrl.offset + coreConfig.app.limitsToDisplay) > $ctrl.resultsBucket.length ){
                    for(var i=$ctrl.offset;i<$ctrl.resultsBucket;i++){
                        file = $ctrl.resultsBucket[i].detail.file_pdf;
                        tanggal = file.substring(8, 10);
                        bulan = file.substring(5, 7);
                        tahun = file.substring(0, 4);
                        file_url = 'http://pdf.antara-insight.id/pdf_images/' + tahun + '/' + bulan + '/' + tanggal + '/' + file;
                        $ctrl.resultsBucket[i].detail.file_pdf = file_url;

                        $ctrl.model.post.posts.push($ctrl.resultsBucket[i].detail);
                    } console.log('banyak: ' + $ctrl.model.post.posts.length);
                    $ctrl.offset = 0;
                } else{
                    for(var i=$ctrl.offset;i<coreConfig.app.limitsToDisplay;i++){
                        file = $ctrl.resultsBucket[i].detail.file_pdf;
                        tanggal = file.substring(8, 10);
                        bulan = file.substring(5, 7);
                        tahun = file.substring(0, 4);
                        file_url = 'http://pdf.antara-insight.id/pdf_images/' + tahun + '/' + bulan + '/' + tanggal + '/' + file;
                        $ctrl.resultsBucket[i].detail.file_pdf = file_url;

                        $ctrl.model.post.posts.push($ctrl.resultsBucket[i].detail);
                    } console.log('banyak: ' + $ctrl.model.post.posts.length);

                    // $ctrl.resultsToDisplay.concat(tmpArr);
                    $ctrl.offset += coreConfig.app.limitsToDisplay;
                }


                // init media player
                // $ctrl.prop.mediaplayer.post = {
                //     type: $ctrl.model.post.posts[0].type,
                //     src: $ctrl.model.post.posts[0].file_pdf
                // };
            }

            $YNC.latencyTest = 100
            $ync($scope, ['categorySelection', 'periodSelection', 'mediaTypeSelection'], coreConfig.app.socketServer);
        }
    ]
});
