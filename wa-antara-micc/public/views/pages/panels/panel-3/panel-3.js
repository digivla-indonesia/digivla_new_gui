'use strict';
angular.module('view.page.panels.panel-3', []);
angular.module('view.page.panels.panel-3').component('pPanel3', {
    templateUrl: 'views/pages/panels/panel-3/panel-3.html',
    controller: ['$timeout', '$interval', '$window', '$document', '$element', 'modelMention', 'coreHelperChart', 'coreHelper',
        'coreHelperColor', 'coreConfig',
        function($timeout, $interval, $window, $document, $element, modelMention, coreHelperChart, coreHelper,
             coreHelperColor, coreConfig
        ) {
            var $ctrl = this;
            $ctrl.wordCloudInit = false;
            $ctrl.interval = [];
            $ctrl.timeout = [];
            $ctrl.model = {
                mention: {},
                top_posts: []
            };
            $ctrl.$onInit = function (){
                $ctrl.model.mention.words = {};
                modelMention.getWords(onMentionwords);
                $ctrl.wordsUpdate();

                modelMention.getPanelData([2, 'topposts'], onTopPost);
                $ctrl.topPostsUpdate();
            };
            $ctrl.$onDestroy = function () {
                $interval.cancel($ctrl.interval[0]);
                $interval.cancel($ctrl.interval[1]);
                $timeout.cancel($ctrl.timeout[0]);
            };
            $ctrl.wordsUpdate = function () {
                $ctrl.interval[0] = $interval(function () {
                    modelMention.getWords(function (words) {
                        onMentionwords(words);
                    });
                }, coreConfig.app.refreshInterval);
            };

            $ctrl.topPostsUpdate = function(){
                $ctrl.interval[1] = $interval(function () {
                    modelMention.getPanelData([2, 'topposts'], onTopPost);
                }, coreConfig.app.refreshInterval);
            };

            $ctrl.storeImage = function ($element) {
                coreHelper.initReport();
                var canvas = $element.find(".c-word-cloud__canvas-hidden")[0];
                var localReport = JSON.parse($window.localStorage.getItem('report'));
                //console.log(canvas.toDataURL());
                localReport.images.wordcloud = { base64: canvas.toDataURL() };
                $window.localStorage.setItem('report', JSON.stringify(localReport));
            };

            $ctrl.storeTS = function (ts) {
                coreHelper.initReport();
                var localReport = JSON.parse($window.localStorage.getItem('report'));
                localReport.stats.ts = ts;
                $window.localStorage.setItem('report', JSON.stringify(localReport));
            };

            function onTopPost(top_posts){
                $ctrl.model.top_posts = top_posts;
                $ctrl.storeTS($ctrl.model.top_posts);
            }

            function onMentionwords(words) {
                //console.log(words);
                $ctrl.model.mention.words = words;

                var canvas = $element.find(".c-word-cloud__canvas")[0];
                var canvasHidden = $element.find(".c-word-cloud__canvas-hidden")[0];
                if (!$ctrl.wordCloudInit) {
                    WordCloud(canvas, {
                        list: words.words,
                        wait: 40,
                        fontFamily: '"proxima_nova_regular", Georgia, sans-serif',
                        drawOutOfBound: false,
                        ellipticity: 0.75,
                        gridSize: 0,
                        shuffle: false,
                        weightFactor: function (size) {
                            return coreHelperChart.calcWordWeight(words, size, canvas);
                        },
                        color: function () {
                            var colors = coreHelperColor.randomizeColors(['#fa941f', '#f72770', '#a3df2c', '#64d6ec', '#a280fc']);
                            return colors[0];
                        }
                    });

                    // Hidden wordcloud for auto generate report
                    WordCloud(canvasHidden, {
                        list: words.words,
                        wait: 10,
                        fontFamily: '"proxima_nova_regular", Georgia, sans-serif',
                        drawOutOfBound: false,
                        ellipticity: 1,
                        shape: 'square',
                        gridSize: 0,
                        rotateRatio: 0,
                        rotationSteps: 1,
                        shuffle: false,
                        weightFactor: function (size) {
                            return coreHelperChart.calcWordWeight(words, size, canvas, 650);
                        },
                        color: function () {
                            var colors = coreHelperColor.randomizeColors(['#fa941f', '#f72770', '#a3df2c', '#64d6ec', '#a280fc']);
                            return colors[0];
                        }
                    });
                    $ctrl.interval[0] = $interval(function () {
                        $ctrl.storeImage($element);
                        $timeout.cancel($ctrl.timeout[0]);
                    }, 10 * 200);


                    $ctrl.wordCloudInit = true;
                }
            }
        }
    ]
});
