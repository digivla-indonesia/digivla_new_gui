'use strict';
angular.module('model.test', []);
angular.module('model.test').factory('modelTest', ['coreConfig',
    function(coreConfig) {
        var self = this;
        // self.test = function (params, success, error) {
        self.test = function (params) {
            // var url = '/posts';
            var url = coreConfig.app.altegraLabsAPI + params.url;
            // axios.get(coreConfig.app.baseUrl + url).then(success).catch(error);
    //         axios({
    //         	method: params.method,
				// url: url,
				// data: params.data
    //         }).then(success).catch(error);
    		axios({
    			method: params.method, 
    			url: url,
    			data: params.data
    		});
        };

        return self;
    }
]);
