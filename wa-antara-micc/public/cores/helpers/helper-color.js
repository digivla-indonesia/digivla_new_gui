'use strict';
angular.module('core.helper.color', []);
angular.module('core.helper.color').service('coreHelperColor', [
    function() {
        var service = this;
        service.randomizeColors = function (array) {
            var currentIndex = array.length, temporaryValue, randomIndex;
            while (0 !== currentIndex) {
                randomIndex = Math.floor(Math.random() * currentIndex);
                currentIndex -= 1;
                temporaryValue = array[currentIndex];
                array[currentIndex] = array[randomIndex];
                array[randomIndex] = temporaryValue;
            }
            return array;
        };
        service.createColors = function (length, type, random) {
            var colors = [];
            var step = (length > 360 ? 1 : Math.ceil(360/length));
            var hue = 0;
            for(var i = 0; i < length; i++) {
                if (i % 360 === 0) {
                    hue = -1;
                }
                hue += step;
                if (type === 'rgba') {
                    var toRGBA = 'rgba(' + service.hslToRgb(hue/360, 0.85, 0.5).join(',') + ',1)';
                    colors.push(toRGBA);
                    console.log(toRGBA);
                }
                else {
                    colors.push('hsla('+ hue +',85%,50%,1)');
                }
            }
            if (random === false) {
                return colors;
            }
            return service.randomizeColors(colors);

        };
        service.hslToRgb = function (h, s, l) {
            var r, g, b;

            function hue2rgb(p, q, t) {
                if (t < 0) t += 1;
                if (t > 1) t -= 1;
                if (t < 1/6) return p + (q - p) * 6 * t;
                if (t < 1/2) return q;
                if (t < 2/3) return p + (q - p) * (2/3 - t) * 6;
                return p;
            }

            if (s === 0) {
                r = g = b = l; // achromatic
            } else {
                var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
                var p = 2 * l - q;

                r = hue2rgb(p, q, h + 1/3);
                g = hue2rgb(p, q, h);
                b = hue2rgb(p, q, h - 1/3);
            }

            return [ Math.round(r * 255), Math.round(g * 255), Math.round(b * 255) ];
        };
        return service;
    }
]);
