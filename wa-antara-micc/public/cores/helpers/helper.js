'use strict';
angular.module('core.helper', ['core.helper.chart', 'core.helper.color']);
angular.module('core.helper').service('coreHelper', [ '$window',
    function($window) {
        var service = {};
        service.toShortCurrency = function (n, decimal) {
            var ranges = [
                { divider: 1e12 , suffix: 'T' },
                { divider: 1e9 , suffix: 'B' },
                { divider: 1e6 , suffix: 'M' },
                { divider: 1e3 , suffix: 'K' }
            ];
            for (var i = 0; i < ranges.length; i++) {
                if (n >= ranges[i].divider) {
                    var number = 0;
                    if (decimal) {
                        number = (n / ranges[i].divider).toFixed(decimal);
                    } else {
                        number = Math.floor(n / ranges[i].divider);
                    }
                    return number.toString() + ranges[i].suffix;
                }
            }
            return n.toString();
        };
        service.initReport = function () {
            var localReport = JSON.parse($window.localStorage.getItem('report'));
            if (!localReport) {
                localReport = {
                    images: {},
                    stats: {},
                    client: ''
                };
                $window.localStorage.setItem('report', JSON.stringify(localReport));
            }
        };
        return service;
    }
]);

