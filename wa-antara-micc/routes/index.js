var express = require('express');
var router = express.Router();
var path = require('path');
var request = require('request');
var chalk = require('chalk');
var nodeService = require(path.resolve('./models/api-node.model.js'));
var modelOM = require(path.resolve('./models/om.model.js'));
var config = require(path.resolve('./config/env/default.js'));
var mail = require(path.resolve('./nodeMailerWithTemp'));


/* GET home page. */
router.get('/', function(req, res, next) {
	var uriSegment = {
		endpoint: '/client/login',
		method: 'POST',
		token: ''
	};
	var params = config.clientLogin;

	nodeService(uriSegment, params, function(response){
		req.session.token = response.data.data;
		res.sendFile(path.join(__dirname, '../views/index.html'));
	});
});

router.post('/api/digivla-requester', function(req, res, next){
	var body = req.body;
	var url = body.url;
	var method = body.method;
	var datas = body.data;
	var uriSegment = {
		endpoint: url,
		method: method,
		token: req.session.token
	};

	// console.log('options: ' + JSON.stringify(uriSegment));
	// console.log('datas: ' + JSON.stringify(datas));

	nodeService(uriSegment, datas, function(response){
		return res.status(200).send(response)
	});
});

router.get('/api/onlinemonitoring/:client', function(req, res, next){
	var options = {
        client: req.params.client
	};
    modelOM(options, function(response){
		return res.status(200).send(response)
	});
});

router.get('/api/wordcloud/:client', function(req, res, next) {
    var options = {
        endpoint: '/socmed/wordcloud',
        method: 'POST',
        token: req.session.token
    };
    nodeService(options, { client_id: req.params.client }, function(response) {
        return res.status(200).send(response);
    });
});

router.post('/api/get-ews', function(req, res, next){
	var body = req.body;
	return res.status(200).send({});

	var datas = req.body;

	datas.keys = req.user.user_detail.usr_keys;
	datas.uid = req.user.user_detail.usr_uid;
	datas.client_id = req.user.user_detail.usr_client_id;

	request.post({url: "http://mskapi.antara-insight.id/api-new.php", form: datas}, function(err, httpResponse, body){
		var resultVar = JSON.parse(JSON.stringify(body));

		return res.status(200).send(body);
	});
});

// sending email with template goes here
router.post('/api/email-sender', function(req, res, next){
	var datas = req.body;
	// mail.sendPasswordReset('reza.achmad.maulana@gmail.com', 'Ogirima','Joshua Aroke','http://yourdomain.com/some-password-links');
	mail.sendAlert(config.clientEmail, datas);

	return res.status(200).send('done emailing');
});

// send email report
router.post('/api/email-report', function(req, res, next){
	mail.sendReport(req.body);

	return res.status(200).send('Report sent');
});

module.exports = router;
