var express = require('express');
var session = require('express-session');
var MemcachedStore = require('connect-memcached')(session);
var path = require('path');
var config = require('./config/env/default.js');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var routes = require('./routes/index');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// set initial variables
app.locals.title = config.app.title;
app.locals.description = config.app.description;
app.locals.secure = config.secure;
app.locals.keywords = config.app.keywords;
app.locals.logo = config.logo;
app.locals.favicon = config.favicon;

// Passing the request url to environment locals
app.use(function (req, res, next) {
  res.locals.host = req.protocol + '://' + req.hostname;
  res.locals.url = req.protocol + '://' + req.headers.host + req.originalUrl;
  /**/
  res.locals.baseUrl = req.protocol + '://' + req.headers.host + '/';
  // api
  // res.locals.apiAddress = config.apiAddress;
  // res.locals.elasticAddress = config.elasticAddress;
  global.apiAddress = config.apiAddress;
  global.elasticAddress = config.elasticAddress;
  global.digitalOceanAPI = config.digitalOceanAPI;
  global.botApiPost = config.botApiPost;
  global.botApiGet = config.botApiGet;
  global.formedAPI = config.formedAPI;
  global.omAPI = config.omAPI;
  /**/
  next();
});

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));

app.use(logger('dev'));
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ extended: false, limit: '50mb' }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


// session
app.use(session({
  saveUninitialized: true,
  resave: true,
  secret: config.sessionSecret,
  store: new MemcachedStore({
    hosts: config.sessionAddress
  })
}));



// uncomment for development envoirnement or in you do requests to different server
app.use(function(req, res, next){
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'POST, GET, PATCH, DELETE');
  next();
})

app.use('/', routes);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.json({
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.json({
    message: err.message,
    error: {}
  });
});


module.exports = app;
