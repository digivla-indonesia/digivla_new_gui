'use strict';

/**
 * Module dependencies.
 */
var request = require('request');

module.exports = function (uriSegment, datas, callback) {

  var auth = new Buffer('altegralabs' + ':' + 'jSMOWnoPFgsHVpMvz5VrIt5kRbzGpI8u9EF1iFQyJQ=').toString('base64');
  var req = {
    uri: global.formedAPI + uriSegment.endpoint,
    method: uriSegment.method,
    headers: {
        'Authorization': 'Basic ' + auth,
        'x-access-token': uriSegment.token,
        'Content-Type': 'application/json'
    },
    json: datas
  };
  //console.log(req);
  request(req, function(err, httpResponse, body){
    if(err){
      return callback({
        success: false,
        data: err
      });
    }

    var resultVar = JSON.parse(JSON.stringify(body));

    return callback({
      success: true,
      data: resultVar
    });
  });
};
