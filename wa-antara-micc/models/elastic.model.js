'use strict';

/**
 * Module dependencies.
 */
var request = require('request');

module.exports = function (uriSegment, datas, callback) {

  var auth = new Buffer('digivla' + ':' + 'nzVV2$/(zTH~>m3V').toString('base64');
  // var destination = 'http://elastic.antara-insight.id/';
  var destination = global.elasticAddress;

  if(!uriSegment.type){
  	destination += uriSegment.index;
  } else{
  	destination += uriSegment.index + "/" + uriSegment.type + "/_search";
  }

  var req = {
    //uri: "http://202.56.162.38:9200/" + uriSegment.index + "/" + uriSegment.type + "/_search",
    //method: 'POST',
    uri: destination,
    method: uriSegment.method,
    headers: {
        Authorization: 'Basic ' + auth,
        'Content-Type': 'application/json'
    },
    json: datas
  };

  request(req, function(err, httpResponse, body){
    if(err){
      return callback({
        success: false,
        data: err
      });
    }

    var resultVar = JSON.parse(JSON.stringify(body));

    return callback({
      success: true,
      data: resultVar
    });
  });
};
