'use strict';

module.exports = {
  app: {
    title: 'Antara Insight - Media Insight',
    description: 'Full-Stack JavaScript with MongoDB, Express, AngularJS, and Node.js',
    keywords: 'mongodb, express, angularjs, node.js, mongoose, passport',
    googleAnalyticsTrackingID: process.env.GOOGLE_ANALYTICS_TRACKING_ID || 'GOOGLE_ANALYTICS_TRACKING_ID'
  },
  port: process.env.PORT || 3000,
  templateEngine: 'swig',
  sessionSecret: 'altegralabscommandcenter',
  sessionCollection: 'sessions',
  logo: 'modules/core/img/brand/logo.png',
  // favicon: 'modules/core/img/brand/favicon.ico',
  favicon: 'public/asset/favicon.ico',
  // local ip
  apiAddress: 'http://apps.antara-insight.id',
  elasticAddress: 'http://el.antara-insight.id/',
  ipElastic: 'el.antara-insight.id',
  sessionAddress: ['139.59.245.121:11211'],
  botApiPost: 'http://128.199.245.199:9080/',
  botApiGet: 'http://128.199.245.199:9070/',
  digitalOceanAPI: 'http://139.59.245.121:3035',
  //formedAPI: 'http://al-api.antara-insight.id/api/v1.0.0',
  //formedAPI: 'http://localhost:3035/api/v1.0.0',
  formedAPI: 'http://172.16.0.11:3035/api/v1.0.0',
  clientLogin: {
    username: "rkjabar",
    password: "d3680f03559100f525bda4e5c27ad824"
  },
  omAPI: 'http://ridwankamil.onlinemonitoring.id/API',
  clientEmail: 'reza.achmad.maulana@gmail.com'
};
