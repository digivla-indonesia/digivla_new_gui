'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var plumber = require('gulp-plumber');

gulp.task('sass', function () {
    return gulp.src('public/assets/styles/app.scss', {base: "./"})
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'expanded', noCache: true}).on('error', sass.logError))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./'));
});

gulp.task('sass:watch', function () {
    gulp.watch('./public/assets/styles/**/*.scss', ['sass']);
});
